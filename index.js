const express = require('express')
const app = express()
const cors = require('cors')
var port = process.env.PORT || 8080;
app.use(cors())
app.use(express.static('static'))
app.use (express.urlencoded({extended: false}))
app.listen(port, () =>
    console.log(`HTTP Server with Express.js is listening on port: ${port}`))

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/static/index.html');
})

app.get('/age', function (req, res) {
    var data = req.query.data
    var zodiacList = ["Monkey","Rooster","Dog","Pig","Rat","Ox","Tiger","Rabbit","Dragon","Snake","Horse","Goat"];
    if (isNaN(data)) data = "Please enter a valid number!"
    else data = "You are " + (2020 - data) + " years old and you are a " + zodiacList[data%12];
    res.send(data)
})